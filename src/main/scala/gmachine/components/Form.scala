package gmachine.components

import cats.effect.IO
import outwatch.dom._
import outwatch.dom.dsl._
import org.scalajs.dom.console
import org.scalajs.dom.window._
import monix.reactive.{Consumer, Observable}
import outwatch.http.Http

import io.circe.parser.parse
import io.circe.syntax._

import scala.scalajs.js
import scala.scalajs.js.JSON
import gmachine.models.{Diff, GResult, LambdaResult}
import outwatch.http.Http.Response
import monix.execution.Scheduler.Implicits.global

import scala.concurrent.duration._

final case class Form(formNode: (Handler[String], Handler[(List[Diff], Int)]) => IO[VDomModifier]) {
  def node(
    hiddenIO: Handler[String],
    visualizationPropsHandler: Handler[(List[Diff], Int)]
  ): IO[VDomModifier] = formNode(hiddenIO, visualizationPropsHandler)
}

object Form {
  val DEFAULT_CODE = "BEGIN\nPUSHINT 2\nPUSHINT 2\nMUL\nEND"
  val DEFAULT_LAMBDA_CODE = "letrec sqr = \\x -> ((* x) x) in  (sqr 100)"

  private def requestsGCode(urls: Observable[String], code: String): Observable[GResult] = for {
    response <- getResponse(urls, code)
  } yield {
    println("response", response.xhr.responseText)
    parse(response.xhr.responseText)
      .flatMap(_.as[GResult]).getOrElse(GResult(List(), Option("Incorrect JSON"), None))
  }

  def timeoutWait(): Observable[Unit] = {
    Observable.unit.delayExecution(0.seconds)
  }

  private def requestsLambdaCode(urls: Observable[String], code: String): Observable[LambdaResult] = for {
    response <- getResponse(urls, code)
  } yield {
    parse(response.xhr.responseText)
      .flatMap(_.as[LambdaResult]).getOrElse(LambdaResult(List()))
  }

  private def getResponse(urls: Observable[String], code: String): Observable[Response] = {
    val response = Http.post(urls.map(url => Http.Request(
      url,
      headers = Map(
        "Accept" -> "application/json",
        "Content-Type" -> "application/json",
      ),
      crossDomain = true,
      data = JSON.stringify(js.Dictionary(
        "code" -> code,
        "onlyResult" -> false
      ))
    )))
    response
  }

  def createNode(
    modalHandler: Handler[String],
    visualizationPropsHandler: Handler[(List[Diff], Int)]
  ): IO[VDomModifier] = {
    for {
      codeTypeHandler <- Handler.create[String]("gcode")
      codeHandler <- Handler.create[String](DEFAULT_CODE)
    } yield {
      val url = for {
        codeType <- codeTypeHandler
      } yield s"https://gmachine-api.herokuapp.com/$codeType"

      val isIncreaseButtonDisabled = for {
        visualizationProps <- visualizationPropsHandler
      } yield {
        val (diff, currentStep) = visualizationProps
        currentStep == diff.length - 1 || diff.isEmpty
      }

      val isDecreaseButtonDisabled = for {
        visualizationProps <- visualizationPropsHandler
      } yield  {
        val (_, currentStep) = visualizationProps
        currentStep < 0
      }

      div(
        cls := "form",
        form(
          cls := "form__container",
          textArea(
            cls := "form__textarea",
            value <-- codeHandler,
            onChange.value --> codeHandler
          ),
          div(
            cls := "form__buttons",
            button(
              "Отправить",
              cls := "form__button form__button--send",
              tpe := "button",
              onClick.foreach({
                val execution = for {
                  _ <- timeoutWait()
                  codeType <- codeTypeHandler
                  code <- codeHandler
                  response <- (codeType match {
                    case "gcode" => requestsGCode(url, code)
                    case "lambda" => requestsLambdaCode(url, code)
                  })
                } yield {
                  response match {
                    case gResult: GResult => {
                      gResult.result match {
                        case Some(result) => {
                          alert(s"Result: $result")
                          codeHandler.onNext(code)
                          visualizationPropsHandler.onNext(gResult.diff, -1)
                        }
                        case _ => alert("Error!")
                      }
                    }
                    case lambdaResult: LambdaResult => {
                      println(lambdaResult.result)
                      val lambdaCode = response.asInstanceOf[LambdaResult].result.reduce((a,b) => s"$a\n$b")
                      codeHandler.onNext(lambdaCode)
                    }
                  }
                }

                execution.map(_ => codeTypeHandler.onNext("gcode")).runAsyncGetFirst

                ()
              }),
            ),
            button(
              "Рисовать граф",
              cls := "form__button form__button--draw-graph",
              tpe := "button",
              disabled,
              onClick.foreach {
                console.log("Рисовать граф")
              }
            ),
            button(
              for {
                codeType <- codeTypeHandler
              } yield codeType match {
                case "gcode" => "G"
                case "lambda" => "λ"
              },
              cls := "form__button form__button--change-code-type",
              tpe := "button",
              onClick(for {
                codeType <- codeTypeHandler
              } yield {
                codeType match {
                  case "gcode" => "lambda"
                  case "lambda" => "gcode"
                }
              }) --> codeTypeHandler,
              onClick(for {
                codeType <- codeTypeHandler
              } yield codeType match {
                case "gcode" => DEFAULT_CODE
                case "lambda" => DEFAULT_LAMBDA_CODE
              }) --> codeHandler
            ),
            button(
              "Справка",
              tpe := "button",
              cls := "form__button form__button--modal",
              onClick(modalHandler.map {
                case "modal modal--hidden" => "modal"
                case _ => "modal modal--hidden"
              }) --> modalHandler
            ),
            button(
              "+",
              cls := "form__button form__button--more",
              tpe := "button",
              disabled <-- isIncreaseButtonDisabled,
              onClick(for {
                visualizationProps <- visualizationPropsHandler
              } yield {
                val (diff, currentStep) = visualizationProps
                (diff, currentStep + 1)
              }) --> visualizationPropsHandler
            ),
            button(
              "-",
              cls := "form__button form__button--less",
              tpe := "button",
              disabled <-- isDecreaseButtonDisabled,
              onClick(for {
                visualizationProps <- visualizationPropsHandler
              } yield {
                val (diff, currentStep) = visualizationProps
                (diff, currentStep match {
                  case -1 => -1
                  case _ => currentStep - 1
                })
              }) --> visualizationPropsHandler
            ),
          )
        )
      )
    }
  }

  def init(): IO[Form] = IO{
    Form((modalHandler, visualizationPropsHandler) => createNode(modalHandler, visualizationPropsHandler))
  }
}