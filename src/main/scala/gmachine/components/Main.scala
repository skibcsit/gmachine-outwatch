package gmachine.components

import outwatch.dom.dsl._
import outwatch.dom._
import cats.effect.IO
import gmachine.models.Diff
import monix.reactive.{Consumer, Observable}

final case class Main(
  visualization: Observable[VDomModifier],
  form: VDomModifier,
  infoModal: VDomModifier
) {

  def node(): Observable[VDomModifier] = {
    for {
      visualizationNode <- visualization
    } yield {
      VDomModifier(
        div(
          cls := "main",
          visualizationNode,
          form,
          infoModal
        )
      )
    }
  }
}

object Main {
  def init: IO[Main] = for {
    modalHandler <- Handler.create[String]("modal modal--hidden")
    visualizationPropsHandler <- Handler.create[(List[Diff], Int)]((List(), -1))
    form <- Form.init()
    infoModal <- InfoModal.init()
  } yield Main(
    Visualization.createNode(visualizationPropsHandler),
    form.node(modalHandler, visualizationPropsHandler),
    infoModal.node(modalHandler)
  )
}
