package gmachine.components

import monix.reactive.{Observable}
import cats.effect.IO
import outwatch.dom._
import outwatch.dom.dsl.{id, _}
import gmachine.models.Diff
import outwatch.dom.VDomModifier
import sigma.{Edge, GraphData, Node, Sigma}
import dagre.{Dagre, EdgeMutationFn, GraphProps, NodeProps, Graph => dargeGraph}
import monix.execution.Scheduler.Implicits.global


final case class Graph(graphNode: Handler[(List[Diff], Int)] => Observable[VDomModifier])  {
  def node(
    propsHandler: Handler[(List[Diff], Int)],
  ): Observable[VDomModifier] = graphNode(propsHandler)
}

object Graph {
  def createNode(propsHandler: Handler[(List[Diff], Int)]): Observable[VDomModifier] = {
    for {
      props <- propsHandler
    } yield {
      val (diffs, currentStep) = props

      def displayAttrByStep: Int => String = {
        case -1 => "none"
        case _ => "block"
      }

      def drawGraph(diffs: List[Diff], currentStep: Int): Unit = {
        val sigma = new Sigma("graph");

        val dgr = new dargeGraph()
          .setGraph(GraphProps.default())
          .setDefaultEdgeLabel(EdgeMutationFn.default())

        dgr.graph().ranker = "tight-tree"
        println(diffs)
        diffs.slice(0, currentStep + 1).foreach(diff => {
          val add = diff.add
          val remove = diff.remove
          val addNodes = add.nodes
          val removeNodes = remove.nodes
          removeNodes.foreach(node => {
            val needsToUpdate = removeNodes.length == 1 && addNodes == 1 && removeNodes.head.id == addNodes.head.id
            if (needsToUpdate) dgr.removeNode(node.id)
            if (node.from.isDefined && node.to.isDefined) {
              dgr.removeEdge(node.id, node.to.get)
              dgr.removeEdge(node.id, node.from.get)
            }
          })
          addNodes.foreach(node => {
            dgr.setNode(node.id, NodeProps.labeledProps(s"${node.label}($$${node.id})"))
            if  (node.from.isDefined && node.to.isDefined) {
              dgr.setEdge(node.id, node.from.get)
              dgr.setEdge(node.id, node.to.get)
            }
          })
        })
        Dagre.layout(dgr)

        val nodesList: List[Node] = dargeGraph.arrayToList(dgr.nodes()).map(nodeId => {
          val node = dgr.node(nodeId)
          Node(
            id = nodeId,
            label = node.label,
            x = node.x.toInt,
            y = node.y.toInt,
            size = 1,
            color = "#92ee1c"
          )
        })
        val edgeList: List[Edge] = dargeGraph.arrayToList(dgr.edges()).map(edge => {
          Edge(
            id = s"${edge.v}->${edge.w}",
            source = edge.v,
            target = edge.w,
          )
        })

        sigma.graph.clear().read(GraphData(
          edges = edgeList,
          nodes = nodesList
        ))

        sigma.refresh()
      }

      div(
        cls := "graph__wrapper",
        styleAttr <-- propsHandler.map({case (_, currentStep) => s"display: ${displayAttrByStep(currentStep)}"}),
        div(
          className := "graph",
          id := "graph",
          onDomMount.foreach { element => {
            element.innerHTML = ""
            propsHandler.map({ case (diffs, currentStep) => {
              drawGraph(diffs, currentStep)
            }}).runAsyncGetFirst
          }}
        )
      )
    }
  }

  def init(): Observable[Graph] = Observable {
    Graph(propsHandler => createNode(propsHandler))
  }
}