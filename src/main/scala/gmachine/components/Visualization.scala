package gmachine.components

import cats.effect.IO
import gmachine.models.Diff
import outwatch.dom._
import outwatch.dom.dsl._
import monix.reactive.{Consumer, Observable}

final case class Visualization(
    visualizationNode: Handler[(List[Diff], Int)] => Observable[VDomModifier]
  ) {
  def node(
    visualizationPropsHandler: Handler[(List[Diff], Int)],
  ): Observable[VDomModifier] = visualizationNode(visualizationPropsHandler)
}

object Visualization {
  def createNode(visualizationPropsHandler: Handler[(List[Diff], Int)]): Observable[VDomModifier] = {
    for {
      graphNode <- Graph.createNode(visualizationPropsHandler)
      stackNode <- Stack.createNode(visualizationPropsHandler)
      visualizationProps <- visualizationPropsHandler
    } yield {
      val (diff, currentStep) = visualizationProps
      println("on some update")

      def commandClass: (Int, Int) => String = (tabIndex, currentStep) => {
        if (tabIndex == currentStep) {
          "visualization__command visualization__command--active"
        } else {
          "visualization__command"
        }
      }

      def displayAttrByStep: Int => String = {
        case -1 => "none"
        case _ => "block"
      }

      div(
        cls := "visualization",
        graphNode,
        stackNode,
        div(
          cls := "visualization__commands-container",
//          styleAttr := s"display: ${displayAttrByStep(currentStep)}",
          div(
            cls := "visualization__commands",
            styleAttr := s"bottom: ${currentStep * 27}px",
            diff.zipWithIndex.map({case (diffElement, index) => div(
              diffElement.command,
              cls := commandClass(index, currentStep)
            )})
          )
        )
      )
    }
  }

  def init(): IO[Visualization] = IO {
    Visualization(visualizationPropsHandler => createNode(visualizationPropsHandler))
  }
}
