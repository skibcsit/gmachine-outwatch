package gmachine.components

import cats.effect.IO
import monix.reactive.Observable
import outwatch.Handler
import outwatch.dom._
import outwatch.dom.dsl._

final case class InfoModal(modalNode: Handler[String] => VNode) {
  def node(hiddenIO: Handler[String]): VNode = modalNode(hiddenIO)
}

object InfoModal {

  def createNode(classIO: Handler[String]): VNode = {
    div(
      cls <-- classIO,
      div(
        cls := "modal__wrapper",
        div(
          cls := "modal__info-area",
        ),
        div(
          cls := "modal__info",
          div(
            cls := "modal__container",
            div(
              cls := "modal__header-wrapper",
              h1(
                "Добро пожаловать в симулятор G-машины",
                cls := "modal__header"
              ),
              button(
                cls := "modal__close",
                onClick(classIO.map {
                  case "modal modal--hidden" => "modal"
                  case _ => "modal modal--hidden"
                }) --> classIO
              )
            ),
            p(
              "В качестве команд G-машины используется последовательность макросов, приведенная в таблице:",
              cls := "modal__description"
            ),
            table(
              cls := "modal__table table",
              tbody(
                tr(
                  th(
                    "№",
                    cls := "table__head"
                  ),
                  th(
                    "Макрос",
                    cls := "table__head"
                  ),
                  th(
                    "Параметр",
                    cls := "table__head"
                  ),
                  th(
                    "Значение",
                    cls := "table__head"
                  ),
                ),
                tr(
                  th(
                    "1",
                    cls := "table__ceil"
                  ),
                  th(
                    "PUSH",
                    cls := "table__ceil"
                  ),
                  th(
                    "m число",
                    cls := "table__ceil"
                  ),
                  th(
                    "Помещает на вершину стека новый элемент. Его значением является содержимое элемента на m ниже УВС.",
                    cls := "table__ceil"
                  ),
                ),
                tr(
                  th(
                    "2",
                    cls := "table__ceil"
                  ),
                  th(
                    "PUSHINT",
                    cls := "table__ceil"
                  ),
                  th(
                    "i указатель",
                    cls := "table__ceil"
                  ),
                  th(
                    "Помещает указатель на вершину содержащую число.",
                    cls := "table__ceil"
                  ),
                ),
                tr(
                  th(
                    "3",
                    cls := "table__ceil"
                  ),
                  th(
                    "Slide",
                    cls := "table__ceil"
                  ),
                  th(
                    "m число",
                    cls := "table__ceil"
                  ),
                  th(
                    "Копирует УВС в ячейку на m позиций ниже УВС.",
                    cls := "table__ceil"
                  ),
                ),
                tr(
                  th(
                    "4",
                    cls := "table__ceil"
                  ),
                  th(
                    "UPDATE",
                    cls := "table__ceil"
                  ),
                  th(
                    "m число",
                    cls := "table__ceil"
                  ),
                  th(
                    "Копирует содержимое УВС в ячейку на m позиций ниже УВС. УВС выталкивается из стека",
                    cls := "table__ceil"
                  ),
                ),
                tr(
                  th(
                    "5",
                    cls := "table__ceil"
                  ),
                  th(
                    "GET",
                    cls := "table__ceil"
                  ),
                  th(
                    "-",
                    cls := "table__ceil"
                  ),
                  th(
                    "Извлекает содержимое УВС стека. Выталкивает УВС. Помещает извлеченное значение в дамп",
                    cls := "table__ceil"
                  ),
                ),
                tr(
                  th(
                    "6",
                    cls := "table__ceil"
                  ),
                  th(
                    "MKAP",
                    cls := "table__ceil"
                  ),
                  th(
                    "-",
                    cls := "table__ceil"
                  ),
                  th(
                    "Создает альфа вершину, для которой левым и правым указателями являются соответственно УВС и УВС-1. Дважды выталкивает содержимое стека. Вставляет в стек созданный указатель на альфа вершину",
                    cls := "table__ceil"
                  ),
                ),
                tr(
                  th(
                    "7",
                    cls := "table__ceil"
                  ),
                  th(
                    "MKINT",
                    cls := "table__ceil"
                  ),
                  th(
                    "-",
                    cls := "table__ceil"
                  ),
                  th(
                    "Создает константную вершину, значение которой взято с вершины дампа. Выталкивает содержимое вершины дампа. Вставляет ее в стек.",
                    cls := "table__ceil"
                  ),
                ),
                tr(
                  th(
                    "8",
                    cls := "table__ceil"
                  ),
                  th(
                    "ALLOC",
                    cls := "table__ceil"
                  ),
                  th(
                    "m число",
                    cls := "table__ceil"
                  ),
                  th(
                    "Создает m новых пустых вершин, вставляет их последовательно в стек.",
                    cls := "table__ceil"
                  ),
                ),
              )
            )
          )
        )
      )
    )
  }

  def init(): IO[InfoModal] = IO{InfoModal(x => createNode(x))}
}
