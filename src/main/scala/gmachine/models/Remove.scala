package gmachine.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

@JsonCodec final case class Remove(
  edges: List[Node],
  nodes: List[Node],
)

object Remove {
  implicit val removeEncoder: Encoder[Remove] = deriveEncoder
  implicit val removeDecoder: Decoder[Remove] = deriveDecoder
}