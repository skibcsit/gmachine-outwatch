package gmachine.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

@JsonCodec final case class GResult(
  diff: List[Diff],
  err: Option[String],
  result: Option[String],
)

object GResult {
  implicit val gResultEncoder: Encoder[GResult] = deriveEncoder
  implicit val gResultDecoder: Decoder[GResult] = deriveDecoder
}