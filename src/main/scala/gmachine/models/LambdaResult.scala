package gmachine.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

@JsonCodec final case class LambdaResult(
  result: List[String],
)

object LambdaResult {
  implicit val lambdaResultEncoder: Encoder[LambdaResult] = deriveEncoder
  implicit val lambdaResultDecoder: Decoder[LambdaResult] = deriveDecoder
}