package gmachine.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

@JsonCodec final case class Diff(
  add: Add,
  remove: Remove,
  command: String,
  stack: List[Node],
)

object Diff {
  implicit val diffEncoder: Encoder[Diff] = deriveEncoder
  implicit val diffDecoder: Decoder[Diff] = deriveDecoder
}