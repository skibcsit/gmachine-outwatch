package gmachine.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

@JsonCodec final case class Add(
  edges: List[Node],
  nodes: List[Node],
)

object Add {
  implicit val addEncoder: Encoder[Add] = deriveEncoder
  implicit val addDecoder: Decoder[Add] = deriveDecoder
}