package gmachine.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.JsonCodec
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

@JsonCodec final case class Node(
  id: Int,
  label: String,
  from: Option[Int],
  to: Option[Int],
)

object Node {
  implicit val nodeEncoder: Encoder[Node] = deriveEncoder
  implicit val nodeDecoder: Decoder[Node] = deriveDecoder
}
