import IOSupport.IOJSDomTestSuite
import JSSupport.JSDomTestSuite
import utest._
import gmachine.components.{Visualization}
import outwatch.dom.{Handler, OutWatch}
import outwatch.dom.dsl._
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.{Element, Event, console, document, html}

object VisualizationTest extends JSDomTestSuite {
  val tests: Tests = Tests {
    test("Visualization is rendered correctly") {
      val node = Visualization.createNode()
      OutWatch.renderInto("#root", node).unsafeRunSync()
      document.body.firstElementChild.innerHTML ==>
        "<div class=\"visualization\">" +
          "Visualization" +
          "<div class=\"visualization__graph\"></div>" +
          "<div class=\"visualization__commands-container\">" +
            "<div class=\"visualization__commands\"></div>" +
          "</div>" +
        "</div>"
    }
  }
}
