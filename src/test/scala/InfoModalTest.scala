import IOSupport.IOJSDomTestSuite
import JSSupport.JSDomTestSuite
import cats.effect.{IO, Timer}
import utest._
import gmachine.components.{InfoModal, Main}
import outwatch.dom.{Handler, OutWatch}
import outwatch.dom.dsl._
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.{Element, Event, console, document, html}
import scala.concurrent.duration._


object InfoModalTest extends IOJSDomTestSuite {
  val tests: Tests = Tests {
    test("InfoModal is rendered correctly") {
      for {
        infoModalHandler <- Handler.create[String]("modal modal--hidden")
      } yield {
        val node = InfoModal.createNode(infoModalHandler)
        OutWatch.renderInto("#root", node).unsafeRunSync()
        document.body.firstElementChild.innerHTML ==>
          "<div>" +
            "<div class=\"modal__wrapper\">" +
              "<div class=\"modal__info-area\">" +
              "</div>" +
              "<div class=\"modal__info\">" +
                "<div class=\"modal__container\">" +
                  "<div class=\"modal__header-wrapper\">" +
                    "<h1 class=\"modal__header\">" +
                      "Добро пожаловать в симулятор G-машины" +
                    "</h1>" +
                    "<button class=\"modal__close\">" +
                    "</button>" +
                  "</div>" +
                  "<p class=\"modal__description\">" +
                    "В качестве команд G-машины используется последовательность макросов, приведенная в таблице:" +
                  "</p>" +
                  "<table class=\"modal__table table\">" +
                    "<tbody>" +
                      "<tr>" +
                        "<th class=\"table__head\">" +
                          "№" +
                        "</th>" +
                        "<th class=\"table__head\">" +
                          "Макрос" +
                        "</th>" +
                        "<th class=\"table__head\">" +
                          "Параметр" +
                        "</th>" +
                        "<th class=\"table__head\">" +
                          "Значение" +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "1" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "PUSH" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "m число" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                            "Помещает на вершину стека новый элемент. Его значением является содержимое элемента на m ниже УВС." +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "2" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "PUSHINT" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "i указатель" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Помещает указатель на вершину содержащую число." +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "3" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Slide" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "m число" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Копирует УВС в ячейку на m позиций ниже УВС." +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "4" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "UPDATE" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "m число" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Копирует содержимое УВС в ячейку на m позиций ниже УВС. УВС выталкивается из стека" +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "5" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "GET" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "-" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Извлекает содержимое УВС стека. Выталкивает УВС. Помещает извлеченное значение в дамп" +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "6" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "MKAP" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "-" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Создает альфа вершину, для которой левым и правым указателями являются соответственно УВС и УВС-1. Дважды выталкивает содержимое стека. Вставляет в стек созданный указатель на альфа вершину" +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "7" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "MKINT" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "-" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Создает константную вершину, значение которой взято с вершины дампа. Выталкивает содержимое вершины дампа. Вставляет ее в стек." +
                        "</th>" +
                      "</tr>" +
                      "<tr>" +
                        "<th class=\"table__ceil\">" +
                          "8" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "ALLOC" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "m число" +
                        "</th>" +
                        "<th class=\"table__ceil\">" +
                          "Создает m новых пустых вершин, вставляет их последовательно в стек." +
                        "</th>" +
                      "</tr>" +
                    "</tbody>" +
                  "</table>" +
                "</div>" +
              "</div>" +
            "</div>" +
          "</div>"
      }
    }

    test("InfoModal has close button") {
      for {
        infoModalHandler <- Handler.create[String]("modal modal--hidden")
      } yield {
        val node = InfoModal.createNode(infoModalHandler)
        OutWatch.renderInto("#root", node).unsafeRunSync()
        assert(
          document.body.querySelector(".modal__close").tagName == "BUTTON",
        )
      }
    }

    test("InfoModal is hidden after first rendering") {
      for {
        infoModalHandler <- Handler.create[String]("modal modal--hidden")
        node = InfoModal.createNode(infoModalHandler)
        _ = OutWatch.renderInto("#root", node).unsafeRunSync()
        _ <- Timer[IO].sleep(100.millisecond)
      } yield {
        assert(
          document.body.querySelector(".modal--hidden") != null
        )
      }
    }
  }
}
