import IOSupport.IOJSDomTestSuite
import cats.effect._
import utest._
import gmachine.components.Form
import outwatch.dom.{Handler, OutWatch}
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.raw.HTMLTextAreaElement
import org.scalajs.dom.document
import scala.concurrent.duration._

object FormTest extends IOJSDomTestSuite {
  val tests: Tests = Tests {
    test("Form is rendered correctly") {
      for {
        formHandler <- Handler.create[String]("modal modal--hidden")
        node <- Form.createNode(formHandler)
      } yield {
        OutWatch.renderInto("#root", node).unsafeRunSync()
        document.body.firstElementChild.innerHTML ==>
          "<div class=\"form\">" +
            "<form class=\"form__container\">" +
              "<textarea class=\"form__textarea\">" +
              "</textarea>" +
              "<div class=\"form__buttons\">" +
                "<button class=\"form__button form__button--send\" type=\"button\">" +
                  "Отправить" +
                "</button>" +
                "<button class=\"form__button form__button--draw-graph\" type=\"button\" disabled=\"\">" +
                  "Рисовать граф" +
                "</button>" +
                "<button class=\"form__button form__button--change-code-type\" type=\"button\">" +
                "</button>" +
                "<button type=\"button\" class=\"form__button form__button--modal\">" +
                  "Справка" +
                "</button>" +
                "<button class=\"form__button form__button--more\" type=\"button\" disabled=\"\">" +
                  "+" +
                "</button>" +
                "<button class=\"form__button form__button--less\" type=\"button\" disabled=\"\">" +
                  "-" +
                "</button>" +
              "</div>" +
            "</form>" +
          "</div>"
      }
    }

    test("Form has correct buttons") {
      for {
        formHandler <- Handler.create[String]("modal modal--hidden")
        node <- Form.createNode(formHandler)
      } yield {
        OutWatch.renderInto("#root", node).unsafeRunSync()
        assert(
          document.body.querySelector(".form__button--draw-graph").tagName == "BUTTON",
          document.body.querySelector(".form__button--change-code-type").tagName == "BUTTON",
          document.body.querySelector(".form__button--modal").tagName == "BUTTON"
        )
      }
    }

    test("Form textarea has default G code after rendering") {
      for {
        formHandler <- Handler.create[String]("modal modal--hidden")
        node <- Form.createNode(formHandler)
        _ = OutWatch.renderInto("#root", node).unsafeRunSync()
        _ <- Timer[IO].sleep(100.millisecond)
      } yield {
        document.body.querySelector(".form__textarea").asInstanceOf[HTMLTextAreaElement].value ==>
          "BEGIN\nPUSHINT 2\nPUSHINT 2\nMUL\nEND"
      }
    }
  }
}
