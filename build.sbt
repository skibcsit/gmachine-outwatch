//import org.scalajs.linker.interface.ModuleInitializer

name := "gmachine"

version := "0.1"
scalaVersion := "2.13.1"

resolvers += "jitpack" at "https://jitpack.io"

lazy val sigmaScalaJS = RootProject(uri("https://www.gitlab.com/zharkowski/sigma-scala-js.git"))
lazy val dargeScalaJS = RootProject(uri("https://www.gitlab.com/zharkowski/dagre-scala-js.git"))
dependsOn(sigmaScalaJS)
dependsOn(dargeScalaJS)

mainClass := Some("gmachine.js.GMachine")

enablePlugins(ScalaJSPlugin)
enablePlugins(ScalaJSBundlerPlugin)

scalacOptions ++= Seq(
  "-Ymacro-annotations"
)

libraryDependencies ++= Seq(
  "io.monix"                      %% "monix"                  % "3.1.0",
  "org.typelevel"                 %% "cats-core"              % "2.0.0",
  "org.typelevel"                 %% "cats-effect"            % "2.0.0",
  "com.github.OutWatch.outwatch"  %%% "outwatch"              % "584f3f2c32",
  "io.circe"                      %%% "circe-generic"         % "0.13.0",
  "io.circe"                      %%% "circe-literal"         % "0.13.0",
  "io.circe"                      %%% "circe-generic-extras"  % "0.13.0",
  "io.circe"                      %%% "circe-parser"          % "0.13.0",
  "com.beachape"                  %%% "enumeratum"            % "1.5.15",
  "com.chuusai"                   %%% "shapeless"             % "2.3.3",
  "com.lihaoyi"                   %%% "utest"                 % "0.7.4"         % "test"
)

requireJsDomEnv in Test := true

scalaJSUseMainModuleInitializer := true
testFrameworks += new TestFramework("utest.runner.Framework")
